#!/usr/bin/env python

import os
import deepdiff

reqs, reqList = ([] for i in range(2))
reqDict, reqDict2, fin = ({} for i in range(3))

cwd = os.getcwd()
basepath = os.path.dirname(cwd)
constraints = os.path.abspath(os.path.join(basepath, "constraints.txt"))

for root, dirs, files in os.walk(basepath, topdown=False):
    for name in files:
        if name == 'requirements.txt':
            reqs.append(os.path.join(root, name))

reqs.sort()

for i in range(len(reqs)):
    reqList.append(reqs[i].split('/')[len(reqs[i].split('/')) - 2])

reqList.sort()

for i in range(len(reqList)):
    with open(reqs[i], 'r') as f:
        reqDict[reqList[i]] = f.readlines()

for i in range(len(reqList)):
    tmp = {}
    for j in reqDict[reqList[i]]:
        if "==" in j:
            tmp.update({j.split("==")[0].lower(): j.split("==")[1][:-1]})              
        elif ">=" in j:
            tmp.update({j.split(">=")[0].lower(): j.split(">=")[1][:-1]})
        elif "<=" in j: 
            tmp.update({j.split("<=")[0].lower(): j.split("<=")[1][:-1]})
        reqDict2[reqList[i]] = tmp

for i in range(len(reqList)):
    j = i+1
    if j != len(reqList):
        diff = deepdiff.DeepDiff(reqDict2[reqList[i]], reqDict2[reqList[j]])
        if 'values_changed' in diff:
            for k in diff['values_changed'].keys():
                pkgName = k[6:-2]
                if len(diff['values_changed'][k]['new_value'].split('.')) & len(diff['values_changed'][k]['old_value'].split('.')) > 1:
                    if int(diff['values_changed'][k]['new_value'].split('.')[0]) == int(diff['values_changed'][k]['old_value'].split('.')[0]):
                        if int(diff['values_changed'][k]['new_value'].split('.')[1]) > int(diff['values_changed'][k]['old_value'].split('.')[1]):
                            if pkgName in fin.keys():
                                if fin[pkgName] > diff['values_changed'][k]['new_value']:
                                    pass
                                else:
                                    fin[pkgName] = diff['values_changed'][k]['new_value']
                            else:
                                fin[pkgName] = diff['values_changed'][k]['new_value']
                        else:
                            if pkgName in fin.keys():
                                if fin[pkgName] > diff['values_changed'][k]['old_value']:
                                    pass
                                else:
                                    fin[pkgName] = diff['values_changed'][k]['old_value']
                            else:
                                fin[pkgName] = diff['values_changed'][k]['old_value']
                    else:
                        if diff['values_changed'][k]['new_value'] > diff['values_changed'][k]['old_value']:
                            if pkgName in fin.keys():
                                if fin[pkgName] > diff['values_changed'][k]['new_value']:
                                    pass
                                else:
                                    fin[pkgName] = diff['values_changed'][k]['new_value']
                            else:
                                fin[pkgName] = diff['values_changed'][k]['new_value']
                        else:
                            if pkgName in fin.keys():
                                if fin[pkgName] > diff['values_changed'][k]['old_value']:
                                    pass
                                else:
                                    fin[pkgName] = diff['values_changed'][k]['old_value']
                            else:
                                fin[pkgName] = diff['values_changed'][k]['old_value']
                else:
                    if diff['values_changed'][k]['new_value'] > diff['values_changed'][k]['old_value']:
                        if pkgName in fin.keys():
                            if fin[pkgName] > diff['values_changed'][k]['new_value']:
                                pass
                            else:
                                fin[pkgName] = diff['values_changed'][k]['new_value']
                        else:
                            fin[pkgName] = diff['values_changed'][k]['new_value']
                    else:
                        if pkgName in fin.keys():
                            if fin[pkgName] > diff['values_changed'][k]['old_value']:
                                pass
                            else:
                                fin[pkgName] = diff['values_changed'][k]['old_value']
                        else:
                            fin[pkgName] = diff['values_changed'][k]['old_value']


with open(constraints, 'w') as f:
    for k,v in fin.items():
        f.write(k+"=="+v+'\n')
